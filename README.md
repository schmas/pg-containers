# PG Plex Custom Containers

Clone this repo at: `/opt/mycontainers`

```bash
mv /opt/mycontainers /opt/mycontainers_bkp
git clone git@gitlab.com:schmas/pg-containers.git /opt/mycontainers

```

## Pritunl - VPN

Port: 1194
